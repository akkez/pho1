<?php
use Rain\Tpl;

$app->map('/get/search', function () use($data){
	include('cfg.php');
	include('111parsers/parser_tools.php');
	if(!isset($_GET['ids'])){die("no id");}
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);$db->set_charset("utf8");
	$user_id = get_user_id_by_asshole_input($_GET['ids']);
	$tpl = new Tpl;
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("tmp","search");

	$stmt = $db->prepare("SELECT user_id, COUNT(*) FROM main WHERE user_id = ? GROUP BY user_id");
	$stmt->bind_param('d', $user_id);
	$stmt->execute();

	$stmt->bind_result($user_id, $pcnt);
	
	$cnt = 0;
	$code="<ol>";
	while ($stmt->fetch()) {
		$code.=sprintf('<li class="list-group-item">Юзер: <a target="_blank" href="'.$data["dir"].'profile/%d">%d</a> (%d фотографии)</li>' . "\n", $user_id, $user_id, $pcnt);
		$cnt++;
	}
	$code.="</ol>";
	
	if ($cnt == 0)
		$code="(пусто) :(";
		
	$stmt->close();
	$tpl->assign("code",$code);
	$tpl->draw( "layout" );
})->via("POST","GET");

$app->map('/get/friends', function () use($data){
	include('cfg.php');
	include('111parsers/parser_tools.php');
	if(!isset($_GET['id'])){die("no id");}
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);$db->set_charset("utf8");
	$id = get_user_id_by_asshole_input($_GET['id']);
	$error_code = 0;
	$error_msg = '';
	$contents = download("http://api.vk.com/method/friends.get?user_id=$id");
	$contents = json_decode($contents, true);
	if (isset($contents['error'])) {
		$error_code = $contents['error']['error_code'];
		$error_msg = $contents['error']['error_msg'];
	} else {
		$input = $contents['response'];
		$ids_str_q = implode(',', $input);
	}
	$tpl = new Tpl;
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("tmp","friends");
	if ($error_code != 0) { 
		$tpl->assign("error","Ошибка VK.API #{$error_code}: {$error_msg}");
	} else if (count($input) == 0) {
		$tpl->assign("error","У данного пользователя нет друзей или все друзья скрыты");
	} else {
		$code=count($input)." друзей найдены следующие:";
		$stmt = $db->prepare("SELECT DISTINCT m.user_id, CASE WHEN u.display_name IS NULL THEN m.user_id ELSE u.display_name END FROM main m
	LEFT JOIN users u ON (m.user_id = u.user_id) 
	WHERE m.user_id IN ($ids_str_q) ORDER BY m.user_id");
		$stmt->execute();
		$stmt->bind_result($user_id, $display_name);
		$cnt = 0;
		$code.="<ol>";
		$list=Array();
		while ($stmt->fetch()) {
			$code.=sprintf ('<li class="list-group-item col-md-3"><a href="/pho1/profile/%d" id="user-%d">%s</a></li>' . "\n", $user_id,$user_id, $display_name);
			$cnt++;
			$list[]=$user_id;
		}
		$code.="</ol>";
		
		if ($cnt == 0)
			$code="<p>К сожалению, база маленькая. Никого не нашли.</p>";
			
	$stmt->close();
	}
	$tpl->assign("code",$code);
	$tpl->assign("list",implode(",",$list));
	$tpl->draw( "layout" );
	
})->via("POST","GET");
?>