<?php
use Rain\Tpl;
$app->get('/api/search', function () use($data){
	include('cfg.php');
	include('111parsers/parser_tools.php');
	if(!isset($_GET['ids'])){die(json_encode(Array("error"=>"no id")));}
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);$db->set_charset("utf8");
	$user_id = get_user_id_by_asshole_input($_GET['ids']);
	$stmt = $db->prepare("SELECT user_id, COUNT(*) FROM main WHERE user_id = ? GROUP BY user_id");
	$stmt->bind_param('d', $user_id);
	$stmt->execute();
	$stmt->bind_result($user_id, $pcnt);
	$list=Array();
	$cnt = 0;
	while ($stmt->fetch()) {
		$list[]=Array("uid"=>$user_id,"link"=>$data["dir"].'profile/'.$user_id,"count"=>$pcnt);
		$cnt++;
	}
	if ($cnt == 0)
		$list=array();
		
	$stmt->close();
	echo json_encode(Array("response"=>$list));
	die;
});


$app->get('/api/friends', function () use($data){
	include('cfg.php');
	include('111parsers/parser_tools.php');
	if(!isset($_GET['ids'])){die(json_encode(Array("error"=>"no id")));}
	$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);$db->set_charset("utf8");
	$id = get_user_id_by_asshole_input($_GET['ids']);
	$error_code = 0;
	$error_msg = '';
	$contents = download("http://api.vk.com/method/friends.get?user_id=$id");
	$contents = json_decode($contents, true);
	if (isset($contents['error'])) {
		$error_code = $contents['error']['error_code'];
		$error_msg = $contents['error']['error_msg'];
	} else {
		$input = $contents['response'];
		$ids_str_q = implode(',', $input);
	}
	if ($error_code != 0) { 
		$error="Ошибка VK.API #{$error_code}: {$error_msg}";
	} else if (count($input) == 0) {
		$error="У данного пользователя нет друзей или все друзья скрыты";
	} else {
		$list=Array("friends"=>count($input));
		$stmt = $db->prepare("SELECT DISTINCT m.user_id, CASE WHEN u.display_name IS NULL THEN m.user_id ELSE u.display_name END FROM main m
	LEFT JOIN users u ON (m.user_id = u.user_id) 
	WHERE m.user_id IN ($ids_str_q) ORDER BY m.user_id");
		$stmt->execute();
		$stmt->bind_result($user_id, $display_name);
		$cnt = 0;
		while ($stmt->fetch()) {
			$cnt++;
			$list["items"][]=Array("uid"=>$user_id,"link"=>$data["dir"].'profile/'.$user_id,"name"=>$display_name);
		}
		
		if ($cnt == 0)
			$error="К сожалению, база маленькая. Никого не нашли.";
			
	$stmt->close();
	}
	if(isset($error)){
		die(json_encode(Array("error"=>$error)));
	}else{
		die(json_encode(Array("response"=>$list)));
	}
});

?>