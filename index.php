<?php
require "inc/raintpl/library/Rain/autoload.php";
require 'inc/slim/Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
include('cfg.php');
$db = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_dbname);
$db->set_charset("utf8"); 
use Rain\Tpl;
$config = array(
    "tpl_dir"       => "templates/",
    "cache_dir"     => "cache/",
    "debug"         => false, // set to false to improve the speed
    "auto_escape"	=> false
);
Tpl::configure( $config );
$data=Array(
	"dir"=>"/pho1/"
);

include("inc/main.php");	//main func - invites,add_group
include("inc/utils.php");	//profile & sazha
include("inc/search1.php"); //friends & search
include("inc/search2.php"); //groups & g2
include("inc/city.php"); //groups
include("inc/api.php"); //api


$app->notFound(function () use ($app) {
	echo "<h2>404 error</h2>";
	echo "sorry!";
});

$app->get('/', function () use($data){
	$tpl = new Tpl;
	$tpl->assign("dir",$data["dir"]);
	$tpl->assign("tmp","in");
	$tpl->draw( "layout" );
	
});


$app->run();
?>